package dir

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

func WalkDir(dir string, fileSize chan<- int64) {
	for _, entry := range dirents(dir) {
		if entry.IsDir() {
			subDir := filepath.Join(dir, entry.Name())
			WalkDir(subDir, fileSize)
		} else {
			fileSize <- entry.Size()
		}
	}
}

func dirents(dir string) []os.FileInfo {
	entries, err := ioutil.ReadDir(dir)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return entries
}

func PrintDiskUsage(nfiles, nbytes int64) {
	fmt.Printf("%d file %.1f GB\n", nfiles, float64(nbytes)/1e9)
}
