package main

import (
	"fmt"
	"time"
)

func main() {

	ticker := time.NewTicker(time.Second * 3)
	go func() {
		for {
			select {
			case <-ticker.C:
				ticker.Reset(time.Second * 10)
				fmt.Println("haha")
			default:
				break
			}
		}
	}()

	time.Sleep(1000000e9)
}
