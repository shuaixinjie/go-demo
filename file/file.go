package file

import (
	"bufio"
	"fmt"
	"os"
)

// ReadLineTimes 读取文件每行出现的次数
func ReadLineTimes(path string) {
	counts := make(map[string]int)
	f, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
	}
	input := bufio.NewScanner(f)
	for input.Scan() {
		counts[input.Text()]++
	}
	for line, n := range counts {
		fmt.Printf("%s:%d\n", line, n)
	}
}
