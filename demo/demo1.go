package demo

import "fmt"

// golang中函数的各种使用方式

// 普通函数
func f1() {}

// 匿名函数，多用于实现回调函数和闭包
func f2() {
	func() {
	}()
}

// 闭包，闭包=函数+引用环境，闭包的变量会被外部引用保存，但无法直接修改，有一定的内存消耗
func add(x int) func(int) int {
	return func(y int) int {
		x += y
		return x
	}
}
func Test()  {
	var f = add(10)
	fmt.Println(f(10)) //20
	fmt.Println(f(20)) //40
	fmt.Println(f(30)) //70

	f1 := add(20)
	fmt.Println(f1(40)) //60
	fmt.Println(f1(50)) //110
}

// 函数作为变量处理
type addFunc func(a, b int) int

func f3(a addFunc) int {
	return a(1, 2)
}
func f4() addFunc {   // 细心的发现，函数作为返回值的时候就是闭包了
	return func(a, b int) int {
		return a+b
	}
}