package demo

import (
	"math/rand"
	"time"
)

// 随机数

// 一般会设置种子，不设置的话默认是1，每次程序重新都会生成一样的结果
func init()  {
	rand.Seed(time.Now().Unix())
}

// 一些使用方法
func test1()  {
	rand.Int63n(66)
	rand.Int63()
}

// 一个做测试比较好用的方法，返回随机切片
func test2()  {
	rand.Perm(10) // 这个随机切片长度为10，值为0-9
}

// 如果想用并发的方式去生成随机数，直接调用rand是适得其反的（内部加了锁），应该为每一个协程new一个新的Rand实例
// 还需要注意的是不通实例随机数种子给一样的话，生成的随机数也会一样
func test3()  {
	rand.New(rand.NewSource(time.Now().UnixNano()))
}
